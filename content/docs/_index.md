---
title: Overview
date: '2018-11-28T05:14:39.000Z'
weight: 1
---

## Bom Resolver Overview

The BOM Resolver could be used both as a [tool](https://bomresolver.io/tool.txt) and a [service](https://services.lammda.se/resolver/alpine/v1/ui) which make it useful for both the single developer and a large enterprises.

The name resolver indicates that this project is about backtracking the Alpine eco system.

The difference between this project and other projects such as [Wolfi](https://edu.chainguard.dev/open-source/wolfi/overview/) project is the compliance with [Alpine build formats](https://wiki.alpinelinux.org/wiki/APKBUILD_Reference).
