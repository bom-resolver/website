---
title: Flow
date: '2019-02-11T09:27:37.000Z'
weight: 2
---

Red indicates software retrieved from Internet that not have been analyzed, green represented analyzed source being rebuilt. The resolver consist of two containers.

* Build a docker image by specifying list of packages ( “packages”)
* Use the metadata created ( aggregated.json ) together with Alpine build manifest to created the resolved.json

Mirroring of source and binaries is optional, but dont expose developers using FOSS.

[Alpine Linux](https://www.alpinelinux.org/) aggregates open source projects by

* Compile source code into binaries
* Provides binaries as packages
* Support for many architectures
  * X86
  * ARM

Without aggregation, developers must build the binaries from source on their own. For embedded systems frameworks such as [Buildroot](https://buildroot.org/), [Yoctoproject](https://www.yoctoproject.org/) and [PTXDIST](https://www.ptxdist.org/) exists.

Repository

Public binary packages [http://dl-cdn.alpinelinux.org/alpine](http://dl-cdn.alpinelinux.org/alpine)

Mirror of public repository [https://services.lammda.se/alpine/public/](https://services.lammda.se/alpine/public/)

Compliant packages [https://services.lammda.se/alpine/approved/](https://services.lammda.se/alpine/approved/)
