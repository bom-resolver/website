# Hugo BOM Theme

Bom is a minimal documentation theme built for Hugo. The design and functionality is intentionally minimal. Original theme is based on Whisper by Robert Austin.

## Theme features

### Content Types

- Docs (Markdown)
- Homepage

### Content Management

- This theme generates documentation from markdown files located in `content/docs`
- The "Home" page is not documentation, it can be used to introduce your project etc.

### Features

- Beautiful and clean typography for all semantic HTML elements

### SCSS

- SCSS (Hugo Pipelines)
- Responsive design
- Bootstrap 4 grid and media queries only

### Speed

- 100/100 Google Lighthouse speed score
- 21KB without images ⚡
- Vanilla JS only

### Menu

- Responsive mobile menu managed in `config.toml`

### Content

- Documentation examples included, using all markdown syntax

# Installation

To use this theme you will need to have Hugo installed. If you don't already have Hugo installed please follow the official [installation guide](https://gohugo.io/getting-started/installing/)

### Check Hugo version (Hugo 0.51+ Extended is required)

This theme uses [Hugo Pipes](https://gohugo.io/hugo-pipes/scss-sass/) to compile SCSS and minify assets. Please make sure you have the **Hugo Extended** version installed. If you are not using the extended version this theme will not not compile.

To check your version of Hugo, run:

```
hugo version
```

This will output the currently installed version of Hugo. Make sure you see `/extended` after the version number, for example `Hugo Static Site Generator v0.51/extended darwin/amd64 BuildDate: unknown` You do not need to use version v0.51 specifically, you can use any version of Hugo above 0.51. It just needs to have the `/extended` part


### Run Hugo

After installing the theme for the first time, generate the Hugo site.

You run this command from the root folder of your Hugo site i.e. `mynewsite/`

```
hugo
```

For local development run Hugo's built-in local server.

```
hugo server
```

Now enter [`localhost:1313`](http://localhost:1313) in the address bar of your browser.

### Using TinaCMS locally

Installing TinaCMS
```
npx @tinacms/cli@latest init
```

Starting TinaCMS
```
npx tinacms dev -c "hugo server -D -p 1313"
```

Access CMS 
```
http://localhost:1313/admin
```